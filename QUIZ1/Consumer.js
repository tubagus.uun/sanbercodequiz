import React, { useContext } from 'react';
import {
    StyleSheet,
    Platform,
    View,
    Text,
    FlatList,
} from 'react-native';

import { RootContext } from "./Soal2"


const Item = ({ item, indek }) => {
    console.log(item)
    return (
        <View style={styles.vtodolist}>
            <View style={styles.vnote}>
                <Text style={styles.tnote}> {item.name} </Text>
                <Text style={styles.tnote}> {item.position} </Text>
            </View>
        </View>
    );
}

const App = () => {

    const state = useContext(RootContext);

    const renderItem = ({ item, index }) => {
        return (
            <Item item={item} indek={index} />
        );
    };

    return (
        <View style={styles.container}>
            <FlatList
                data={state.name} renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        ...Platform.select({
            ios: {
                padding: 10,
                paddingTop: 20
            },
            android: {
                padding: 10,
            },
            default: {
                padding: 5,
            }
        })
    },
    bhapus: {

    },
    vtodolist: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 4,
        padding: 10,
        paddingVertical: 20,
        paddingRight: 25,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: 'grey',
    },
    vnote: {
        // borderWidth: 1,
    },
    tnote: {
        fontSize: 11
    },
});

export default App;